#!/usr/local/bin/python3.5
# -*- coding: utf-8 -*-

"""
It basically works. You can use the example in this script.
It is still not perfect cause there is a case when an out of range error
emerges, but I think it is a good proof of concept and I can improve it
to be perfect if you hire me ;)
For sure I'd have to stress test it a lot cause there are many boundary
conditions.
"""


def match(pattern, text):
    """
    The main function to use
    :param pattern: string
    :param text: string
    :return: boolean
    """
    patterns = pattern.split('*')

    for pattern in patterns:
        scope = len(pattern)

        if len(text) == 0:
            return False

        for ixt in range(len(text)):
            if match_second_pattern(pattern, text[ixt:scope + ixt]):
                text = text[scope + ixt:]
                break

    if len(text) > 0:
        return False
    else:
        return True


def match_second_pattern(pattern, text):
    """
    It checks only pattern with '.' and '?'
    :param pattern: text
    :param text: text
    :return: boolean
    """
    ip = 0
    it = 0

    while it < len(text):
        t = text[it]
        p = pattern[ip]

        if p == t:
            it += 1
            ip += 1
            continue
        elif p == '.':
            it += 1
            ip += 1
            continue
        elif p == '?':
            if pattern[ip + 1] == t:
                it += 1
                ip += 2
                continue
            else:
                it += 1
                ip += 1
                continue
        else:
            return False

    return True


if __name__ == "__main__":
    pattern1 = "ab*cd*kkk.a?b"
    text1 = "abszcdqqkkkzaxb"

    print(match(pattern1, text1))
