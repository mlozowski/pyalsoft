# Python backend test
Backend part for Alsoft.

## 1. Text pattern
Assumption: strings to match contain only letters 'a'-'z'
Write a Python function match that will take a pattern and a string as arguments and will determine if the string matches the pattern (without using re or equivalents). 
The performance is not important, however try to describe potential problems that the proposed function may have (in terms of performance, etc.).
Examples:
```
        match("abcd", "abcd")
        >>> true
        match("abcd", "abzcd")
        >>> false
        match("ab.cd", "abcd")
        >>> false
        match("ab.cd", "abzcd")
        >>> true
        match("ab?cd", "abcd")
        >>> true
        match("ab?cd", "abzcd")
        >>> true
        match("ab?cd", "abxyzcd")
        >>> false
        match("ab\*cd", "abcd")
        >>> true
        match("ab\*cd", "abzcd")
        >>> true
        match("ab\*cd", "abxyzcd")
        >>> true
        match("ab\*cd", "abzcdz")
        >>> false
```

## 2. Text manipulation
Text manipulation in Python (please provide the production unit test, too)
Create a Python class that will take some text (words separated by spaces) as an argument:
manipulator = TextManipulator("un deux trois un deux trois")
It should be shown correctly:

```
repr(manipulator)
>>> "un deux trois un deux trois"
manipulator
>>> un deux trois un deux trois
The syntax manipulateur["mot"] must give a list of positions of the word in the list:
manipulator["deux"]
>>> [1, 4]
The syntax manipulateur.mot must allow to access to the following functions:
replace replaces a word
manipulator.deux.replace("second")	
manipulator
>>> un second trois un second trois
remove removes the specified word
        		manipulator.un.remove()
        		manipulator
        		>>> second trois second trois
    	insert inserts the word at the specified position:
        		manipulator.quatre.insert(2)
        		manipulator
        		>>> second trois quatre second trois
```