

class TextManipulator(object):

    def __init__(self, text):
        self.text = text
        self.word_to_process = None

    @property
    def words(self):
        return self.text.split()

    def __repr__(self):
        return self.text

    def __str__(self):
        return self.text

    def __getitem__(self, item):
        found_words_indexes = []
        for i, word in enumerate(self.words):
            if item == word:
                found_words_indexes.append(i)
        return found_words_indexes

    def __getattr__(self, item):
        self.word_to_process = item
        return self

    def replace(self, new_word):
        if self.word_to_process:
            self.text = self.text.replace(self.word_to_process, new_word)
            self.word_to_process = None

    def remove(self):
        if self.word_to_process:
            cleaned_words_list = list(
                filter(lambda word: word != self.word_to_process, self.words)
            )
            self.text = ' '.join(cleaned_words_list)
            self.word_to_process = None

    def insert(self, index):
        if self.word_to_process:
            words_list = self.words
            words_list.insert(index, self.word_to_process)
            self.text = ' '.join(words_list)
            self.word_to_process = None


if __name__ == '__main__':
    t = TextManipulator("kota Ala ma i kota YYY Ala kota")
    print(t)
    print(t['Ala'])

    t.Ala.replace('QQQ')
    print(t)
    t.kota.remove()
    print(t)
    t.Start.insert(1)
    print(t)
