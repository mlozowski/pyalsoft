
import unittest

from second.manipulator import TextManipulator


class TestTextManipulator(unittest.TestCase):

    def test_representation(self):
        text = "Some text 123"
        t = TextManipulator(text)
        representation = repr(t)
        self.assertEqual(representation, text)

    def test_replace(self):
        text = "Some digits here 123"
        t = TextManipulator(text)
        t.digits.replace('numbers')
        self.assertEqual(t.text, "Some numbers here 123")

    def test_remove(self):
        text = "Some digits here 123"
        t = TextManipulator(text)
        t.here.remove()
        self.assertEqual(t.text, "Some digits 123")

    def test_insert(self):
        text = "Some digits 123"
        t = TextManipulator(text)
        t.here.insert(2)
        self.assertEqual(t.text, "Some digits here 123")


if __name__ == '__main__':
    unittest.main()
